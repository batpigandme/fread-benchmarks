# Benchmarking reading data from csv files with base R, readr and data.table

This serves as accompanying code for the [How data.table's fread can save you a lot of time and memory, and take input from shell commands](https://jozef.io/r917-fread-comparisons/) blogpost.

## Getting the data

The data source is Airline on-time performance from http://stat-computing.org/dataexpo/2009/the-data.html.

The following will download and extract the data into `~/dataexpo`. The download size is cca. 868 MB in bz2 files. The extracted size is cca 5.34 GB in csv files.

```
Rscript data_prep/data_prep.R
```

## Running the benchmarks

### For base R

```
bash bench/bench.sh rscripts/01_base.R &> results/out_base.txt
```

### For data.table::fread

```
bash bench/bench.sh rscripts/02_fread.R &> results/out_fread.txt
```

### For readr::read_csv

```
bash bench/bench.sh rscripts/03_readr.R &> results/out_readr.txt
```

### For vroom::vroom with purrr::map_dfr

```
bash bench/bench.sh rscripts/06_vroom_purrr.R &> results/out_vroom_purrr.txt
```

### For vroom::vroom on multiple files

```
bash bench/bench.sh rscripts/07_vroom.R &> results/out_vroom.txt
```

### For data.table::fread with grep

```
bash bench/bench.sh rscripts/04_fread_grep.R &> results/out_fread_grep.txt
```

### For readr::read_csv with grep

```
bash bench/bench.sh rscripts/05_readr_grep.R &> results/out_readr_grep.txt
```

## Overview of the results:

| method                                 | max. memory | avg. time |
|----------------------------------------|------------:|----------:|
| `utils::read.csv` + `base::rbind`      | 	 21.70 GB  |   8.13 m  |
| `readr::read_csv` + `purrr::map_dfr`   | 	 27.02 GB  |   3.43 m  |
| `vroom::vroom` + `purrr::map_dfr` * ** | 	 25.70 GB  |   1.67 m  |
| `data.table::fread` + `rbindlist`      | 	 15.25 GB  |   1.40 m  |
| `vroom::vroom` (multiple files) * **   | 	 31.18 GB  |   1.30 m  |
| `data.table::fread` from `grep`        | 	  1.68 GB  |   0.25 m  |
| `readr::read_csv`+ `pipe()` from `grep`| 	  1.70 GB  |   0.88 m  |

- max. memory = Maximum resident set size
- avg. time = Average "real" time as measured by `time`

### Notes

1. (*) note that vroom does not get all advertised speed benefits with the used R version (3.4.4)
2. (**) note the unusually high user time in the [detailed results](results/out_vroom_purrr.txt)


## SessionInfo

- R version 3.4.4 (2018-03-15)
- Platform: x86_64-pc-linux-gnu (64-bit)
- Running under: Ubuntu 18.04.1 LTS

used packages:

- data.table_1.12.2
- readr_1.3.1
- magrittr_1.5
- purrr_0.2.4
- vroom_1.0.1
