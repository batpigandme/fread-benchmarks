suppressPackageStartupMessages({
  library(readr)
})

dataDir <- path.expand("~/dataexpo")
dataFiles <- dir(dataDir, pattern = "csv$", full.names = TRUE)

# All flights by American Airlines
command <- sprintf(
  "grep --text ',AA,' %s",
  paste(dataFiles, collapse = " ")
)

# default would convert first row of `grep` output into column names
col_names <- FALSE

# default would determine some cols wrongly as logical and 
# convert all the values, pre-define explicitly
col_types <- readr::cols(
  col_character(), # this is for the file name returned by `grep`
  col_integer(),
  col_integer(),
  col_integer(),
  col_integer(),
  col_integer(),
  col_integer(),
  col_integer(),
  col_character(),
  col_integer(),
  col_character(),
  col_integer(),
  col_integer(),
  col_integer(),
  col_integer(),
  col_integer(),
  col_character(),
  col_character(),
  col_integer(),
  col_integer(),
  col_integer(),
  col_integer(),
  col_character(),
  col_integer(),
  col_integer(),
  col_integer(),
  col_integer(),
  col_integer(),
  col_integer()
)

df <- readr::read_csv(
  file = pipe(command),
  col_types = col_types,
  col_names = col_names
)
