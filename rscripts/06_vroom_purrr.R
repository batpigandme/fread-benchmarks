suppressPackageStartupMessages({
  library(vroom)
  library(purrr)
  library(magrittr)
})

dataDir <- path.expand("~/dataexpo")
dataFiles <- dir(dataDir, pattern = "csv$", full.names = TRUE)

# rbind_rows won't coerce, prefedine
col_types <- vroom::cols(
  .default = vroom::col_double(),
  UniqueCarrier = vroom::col_character(),
  TailNum = vroom::col_character(),
  Origin = vroom::col_character(),
  Dest = vroom::col_character(),
  CancellationCode = vroom::col_character(),
  CarrierDelay = vroom::col_double(),
  WeatherDelay = vroom::col_double(),
  NASDelay = vroom::col_double(),
  SecurityDelay = vroom::col_double(),
  LateAircraftDelay = vroom::col_double()
)

df <- dataFiles %>%
  purrr::map_dfr(
    vroom::vroom,
    col_types = col_types,
    progress = FALSE,
    num_threads = 8L
  )
