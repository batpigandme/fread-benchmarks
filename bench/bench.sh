#!/bin/bash
scriptf=$1
printf "$scriptf \n\n"

/usr/bin/time -v Rscript $scriptf  \
 2>&1 >/dev/null | \
 grep -E 'Maximum resident'

time for i in {1..10}; do Rscript $scriptf >/dev/null; done
